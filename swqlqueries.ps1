# Ignore SSL trust issues
Add-Type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

# Format and encode credentials
# Note: you may still need to browse to the site from the requesting IP and cancel the PKI authentication popup in order to run queries from PowerShell
$user = 'USERNAME'
$pass = 'PASSWORD'
$pair = "$($user):$($pass)"
$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$basicAuthValue = "Basic $encodedCreds"
$Headers = @{
    Authorization = $basicAuthValue
}

# Get general device information for all managed nodes
Invoke-WebRequest 'https://10.0.0.1:17778/SolarWinds/InformationService/v3/Json/Query?Query=SELECT+NodeID,+ObjectSubType,+NodeDescription,+Description,+Vendor,+MachineType+FROM+Orion.Nodes' -Method GET -Headers $Headers

# Get Node Names and IP Addresses for all managed nodes
((Invoke-WebRequest 'https://10.0.0.1:17778/SolarWinds/InformationService/v3/Json/Query?Query=SELECT+NodeName,+IPAddress+FROM+Orion.Nodes' -Method GET -Headers $Headers).Content | ConvertFrom-Json).results

# Get IPs, User IDs, and Usernames for user login events
Invoke-WebRequest 'https://10.0.0.1:17778/SolarWinds/InformationService/v3/Json/Query?Query=SELECT+IPAddress,+AccountID,+DisplayName+FROM+Orion.UserLogin' -Method GET -Headers $Headers

# Get configs
Invoke-WebRequest 'https://10.0.0.1:17778/SolarWinds/InformationService/v3/Json/Query?Query=SELECT+config+FROM+cirrus.configarchive' -Method GET -Headers $Headers

# List Traps
Invoke-WebRequest 'https://10.0.0.1:17778/SolarWinds/InformationService/v3/Json/Query?Query=SELECT+IPAddress,+Hostname,+DisplayName,+Description+FROM+Orion.Traps' -Method GET -Headers $Headers


